import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Button, SafeAreaView, Text, TextInput, View } from 'react-native';
import { Provider } from 'react-redux';
import {useSelector, useDispatch} from 'react-redux';
import Home from './src/screens/Home';

import store from './src/ducks/store';
import RootNavigation from './src/navigations/RootNavigation';


// export const apiBaseURL = 'https://pro.coinmarketcap.com';

// interface coin {
//     id: number,
//     name: string,
//     symbol: string,
//     max_supply: number,
//     circulating_supply: number,
//     total_supply: number,
//     price: number
// }

const App = () => {

    // const [cryptos, setCryptos] = useState<any>([]);
    // const [coinsToBeSearched, setCoinsToBeSearched] = useState('');

    // const fetchData = async () => {
    //     try {
    //         let qs = `?start=1&limit=2&convert=USD`;
    //         let {data} = await axios.get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest' + qs, {
    //                 headers: { 'X-CMC_PRO_API_KEY': 'ebe83e5e-fa88-410b-82ac-e2a2bcc5017d' }
    //             });
    //         console.log("dataaa", data);
    //         let b = data.data;
    //         const d = b.map((coins: any) => {
    //             const {id, name, symbol, max_supply, circulating_supply, total_supply, quote} = coins;
    //             const new_coin: coin = {
    //                 id,
    //                 name,
    //                 symbol,
    //                 max_supply,
    //                 circulating_supply,
    //                 total_supply,
    //                 price: quote.USD.price
    //             }

    //             return new_coin;
    //         })

    //         console.log('ddddd', d);
    //         setCryptos(d);
            
    //     } catch(error) {
    //         console.log(error);
    //     }
    // }

    // useEffect(() => {
    //     fetchData()
    // }, []);

    // useEffect(() => {
    //     // console.log("Coin values in State: ", cryptos);
    // }, [cryptos])

    return(
        <Provider store={store}>
            <RootNavigation/>
        </Provider>
    );
};

export default App;