import axios from 'axios';

const BASE_URL = "https://pro-api.coinmarketcap.com/";
const REQUEST_TIMEOUT_SECONDS = 5;

const api = axios.create({
    baseURL: BASE_URL,
    timeout: REQUEST_TIMEOUT_SECONDS * 1000,
    headers: {'X-CMC_PRO_API_KEY': 'ebe83e5e-fa88-410b-82ac-e2a2bcc5017d' }
});

export default api;