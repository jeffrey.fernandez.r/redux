import api from "..";

export const getCryptos = () => api.get("v1/cryptocurrency/listings/latest?start=1&limit=50&convert=USD");