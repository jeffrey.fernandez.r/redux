import { actionTypes as types, ICrypto } from "./types";

export const fetchApiStart = () => ({type: types.FETCH_CRYPTOS});

export const fetchingApiDone = (data: ICrypto) => ({
        type: types.FETCH_CRYPTOS_DONE,
        payload: {data}
    });

export const fetchingApiError = (data: string) => ({
    type: types.FETCH_CRYPTOS_ERROR,
    payload: data
})
