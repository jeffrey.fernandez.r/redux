import {actionTypes as types, CryptosAction, CryptosState} from './types';

export const initialState = {
    data: [],
    isLoading: false,
    error: ''
};

const fetchingCryptosDone = (state: CryptosState, action: any) => {
    const { data } = action.payload;
    return { ...state, isLoading: false, data, error: ""};
};

const fetchingCryptosError = (state: CryptosState, action: any) => {
    const {data} = action.payload;
    return { ...state, isLoading: false, error: data};
};

const fetchingCryptosStart = (state: CryptosState, action: any) => {
    return { ...state, isLoading: true};
};

const cyptosReducer = (state = initialState, action: CryptosAction) => {
    switch(action.type) {
        case types.FETCH_CRYPTOS: 
            return fetchingCryptosStart(state, action);
        case types.FETCH_CRYPTOS_DONE: 
            return fetchingCryptosDone(state, action);
        case types.FETCH_CRYPTOS_ERROR:
            return fetchingCryptosError(state, action);
        default:
            return state;
    };
};

export default cyptosReducer; 