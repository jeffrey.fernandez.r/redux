import {takeLatest, put, call} from '@redux-saga/core/effects';
import {getCryptos} from '../../../api/services/cryptos.services';
import { actionTypes, ICrypto } from '../types';
import { fetchingApiDone, fetchingApiError } from '../actionCreators';

function* processFetchItem(action: any) {
    try {
        //api service call
        const {data} = yield call(getCryptos);

        const cryptoCoins = data.data.map((coins: any) => {
            const {id, name, symbol, max_supply, circulating_supply, total_supply, quote} = coins;

            const crypto: ICrypto =  {
                id,
                name,
                symbol,
                max_supply,
                circulating_supply,
                total_supply,
                price: quote.USD.price
            };
            return crypto;
        });
        yield put(fetchingApiDone(cryptoCoins));
    } catch(error) {
        yield put(fetchingApiError(error.message));
    }
};

export default function* watchEvent() {
    yield takeLatest(actionTypes.FETCH_CRYPTOS, processFetchItem)
}