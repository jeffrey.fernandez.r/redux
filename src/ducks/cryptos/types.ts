export const actionTypes = {
    FETCH_CRYPTOS: 'FETCH_CRYPTOS',
    FETCH_CRYPTOS_DONE: 'FETCH_CRYPTOS_DONE',
    FETCH_CRYPTOS_ERROR: 'FETCH_CRYPTOS_ERROR'
};

export interface ICrypto {
    id: number,
    name: string,
    symbol: string,
    max_supply: number,
    circulating_supply: number,
    total_supply: number,
    price: number
};

export type CryptosState = {
    data: Array<ICrypto>
    isLoading: boolean
    error: string
};

export type CryptosAction = 
    | { type: 'FETCH_CRYPTOS'}
    | { type: 'FETCH_CRYPTOS_DONE'}
    | { type: 'FETCH_CRYPTOS_ERROR'}