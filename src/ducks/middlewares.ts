import createSagaMiddleware from '@redux-saga/core';

export const sagaMiddleware = createSagaMiddleware();
export const middlewares = [sagaMiddleware];