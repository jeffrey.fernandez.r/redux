import { combineReducers } from "redux";
import cyptosReducer from './cryptos/reducer';

const reducers = combineReducers({cyptosReducer});

export default reducers;