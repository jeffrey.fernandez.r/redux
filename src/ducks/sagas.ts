import { all } from "@redux-saga/core/effects";
import cryptoSaga from './cryptos/saga';

export default function* sagas(){
    yield all([cryptoSaga()]);
};