import { createStore, applyMiddleware} from "redux";
import logger from 'redux-logger';
import main from "./cryptos/saga";
import {middlewares, sagaMiddleware} from './middlewares';

import reducers from "./reducers";

const store = createStore(
    reducers,
    applyMiddleware(logger, ...middlewares)
);

sagaMiddleware.run(main)

export default store;