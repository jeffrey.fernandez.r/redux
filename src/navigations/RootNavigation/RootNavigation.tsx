import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import Home from '../../screens/Home';
import CryptoDetails from '../../screens/CryptoDetails';

const Stack = createStackNavigator();

const RootNavigation = (props:any) => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="Details" component={CryptoDetails}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default RootNavigation;