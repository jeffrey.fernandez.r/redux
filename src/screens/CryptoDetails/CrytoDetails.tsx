import React from 'react';
import {Text, View} from 'react-native';
import { useSelector } from 'react-redux';

import { cryptosSelectors } from '../../ducks/cryptos';
import { ICrypto } from '../../ducks/cryptos/types';

const CryptoDetails = ({route}: any) => {
    const {data} = useSelector(cryptosSelectors);
    const details = data.find((crypto: ICrypto) => crypto.id === route.params.id)
    return (
        <View style={{marginTop: 10, paddingLeft: 10}}>
            <Text>{`PRICE:  ${details.price}`}</Text>
            <Text>{`CIRCULATING SUPPLY:  ${details.circulating_supply}`}</Text>
            {details.max_supply && <Text>{`MAX SUPPLY:  ${details.max_supply}`}</Text>}
            <Text>{`TOTAL SUPPLY:  ${details.total_supply}`}</Text>
        </View>
    );
};

export default CryptoDetails;