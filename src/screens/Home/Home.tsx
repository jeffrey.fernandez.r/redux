import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, View, Button, TouchableOpacity, FlatList} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';

import { fetchApiStart } from '../../ducks/cryptos/actionCreators';
import { cryptosSelectors } from '../../ducks/cryptos';

const Home = (props: any) => {
    const dispatch = useDispatch();
    const {data, isLoading, error} = useSelector(cryptosSelectors);

    const handleButtonClick = () => {
        dispatch(fetchApiStart());
    };

    const RenderList = ({item}: any) => {
        return (
            <TouchableOpacity
                onPress={() => props.navigation.navigate("Details", {
                    id: item.id
                })}
            >
            <View style={{margin: 10, backgroundColor: 'teal', paddingLeft: 10, borderRadius: 5}}>
                <Text>{item.name}</Text>
                <Text>{item.symbol}</Text>
                <Text>{`Price: ${item.price}`}</Text>
            </View>
        </TouchableOpacity>);
    };

    return (
        <SafeAreaView>
            <View>
                <Button
                    title="Get Crypto Values"
                    onPress={handleButtonClick}
                />
                <FlatList
                    data={data.sort((a: any, b:any) => b.price - a.price)}
                    keyExtractor={item=>item.id}
                    renderItem={RenderList}
                />
            </View>
        </SafeAreaView>
    );
}

export default Home;

